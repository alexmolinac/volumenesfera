struct vol {
	float radio;
	float p;
	float c;
};
program VOL_PROG {
	version VOL_VERS {
	float VOL(vol) = 1;
	} = 5;
} = 700;

